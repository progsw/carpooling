<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>aboutus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
        <link href="css/theme-style.css" rel="stylesheet">
        <link href="css/custom-style.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
        <div id="undefined-sticky-wrapper" class="sticky-wrapper" style="height: 88px;">
            <nav class="navbar db-navbar" style="">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                        <a href="#"><img src="images/logo6.jpg" alt="logo"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="aboutus.php">AboutUs</a></li>
                            <li><a href="#">Download App</a></li>
                            <li><a href="signup.php"> Sign Up</a></li>
                            <li><a href="login.php"> Log In</a></li>
                        </ul>
                    </div>
                </div>
            </nav></div>
        </header> 
        <main class="main-content">
            <section class="aboutus-content">
                <div class="aboutus-container"> 
                    <div class="text-container">
                         <h1 class="h1-text">Who we are</h1>
                         <p class="aboutus">We are a community who  passionate want to help the environment providing carsharing services.
                             We can get better air quality and lower carbon emissions reducing traffic fumes. 
                             Fewer cars being on the road make less congestion ,shorter journeys, an increased chance of finding a parking space. 
                             Reducing fuel costs and parking fees are beneffits as well. 
                             Journeys can be more pleasant and fun due to having company.
                         </p>
                         <p class="aboutus">Carsharing has never been easier. So, if you share our ideas we will be glad to JOIN US</p>
                    </div>
                </div>                                          
            </section>
        </main>
        <footer>            
            <div class="footer-container">                           
                <span>Stay in Touch</span>                
                <ul class="list-inline">                     
                    <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>                     
                </ul>
                <p>LetsCarpooling | Copyright 2019 &copy;</p> 
            </div>
        </footer>
                   
        <script src="bootstrap/js/bootstrap.min.js"></script>   
    </body>
</html>
