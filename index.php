<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>index</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
        <link href="css/theme-style.css" rel="stylesheet">
        <link href="css/custom-style.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
        <div id="undefined-sticky-wrapper" class="sticky-wrapper" style="height: 88px;">
            <nav class="navbar db-navbar" style="">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                        <a href="#"><img src="images/logo6.jpg" alt="logo"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="aboutus.php">AboutUs</a></li>
                            <li><a href="#">DownloadApp</a></li>
                            <li><a href="signup.php"> Sign Up</a></li>
                            <li><a href="login.php"> Log In</a></li>
                        </ul>
                    </div>
                </div>
            </nav></div>
        </header>    
		
        <div id="content">
            <!--img src="images/city1.jpg" alt="naslovna slika" class="slika"--> 
            <!-- Slideshow container -->
            <div class="slide-container">
            <!-- Full-width slides/quotes -->
                <div class="mySlides">
                    <img src="images/city8.jpg" alt="" class="slika1">
                    <div class="textimg1">Download App</div> 
                    <div class="textimg2">and Join Us</div> 
                    
                </div>
                <div class="mySlides">
                    <img src="images/city8.jpg" alt="" class="slika1">
                    <div class="textimg1">The ride with us</div> 
                    <div class="textimg2">is more fun</div>                     
                </div>                
            <!-- Next/prev buttons -->
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>
            </div>           
                  
            <script src="js/slide.js"></script>           
        </div> 
        <footer>            
            <div class="footer-container">                           
                <span>Stay in Touch</span>                
                <ul class="list-inline">                     
                    <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>                     
                </ul>
                <p>LetsCarpooling | Copyright 2019 &copy;</p> 
            </div>
        </footer>
                   
        <script src="bootstrap/js/bootstrap.min.js"></script>   
    </body>
</html>
